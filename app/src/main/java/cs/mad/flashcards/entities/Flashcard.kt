package cs.mad.flashcards.entities


data class Flashcard(
    var question: String,
    var answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            var cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Definition $i"))
            }
            return cards
        }
    }
}