package cs.mad.flashcards.activities


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.databinding.ActivityStudySetBinding


class StudySetActivity: AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }


}

