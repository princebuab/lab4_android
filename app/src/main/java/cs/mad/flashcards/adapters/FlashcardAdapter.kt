package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
//        viewHolder.flashcardTitle.setOnClickListener {
//            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
//        }
        viewHolder.flashcardTitle.setOnClickListener{
//            var title = viewHolder.flashcardTitle.text
            // https://kotlinlang.org/docs/collection-elements.html#retrieve-with-selector
            var selectedCard = dataSet.find{it.question == viewHolder.flashcardTitle.text}
            showStandardDialog(viewHolder.itemView.context, selectedCard)
        }

        viewHolder.flashcardTitle.setOnLongClickListener{
//            Snackbar.make(viewHolder.itemView, "Long click success!", Snackbar.LENGTH_LONG).show()
            var selectedCard = dataSet.find{it.question == viewHolder.flashcardTitle.text}
            showCustomDialog(viewHolder, LayoutInflater.from(viewHolder.itemView.context), selectedCard)
            true
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}

private fun showStandardDialog(c: Context,card:Flashcard?) {
    AlertDialog.Builder(c)
        .setTitle(card?.question)
        .setMessage(card?.answer)
        .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
            // do something on click
        }
//        .setNeutralButton("edit") { dialogInterface: DialogInterface, i: Int ->
//
//        }
        .setNegativeButton("Delete") { dialogInterface: DialogInterface, i: Int ->

        }
        .create()
        .show()
}
//
private fun showCustomDialog(viewHolder: FlashcardAdapter.ViewHolder,layoutInflater: LayoutInflater,card:Flashcard?) {
    val titleView = layoutInflater.inflate(R.layout.custom_title, null)
    val bodyView = layoutInflater.inflate(R.layout.custom_body, null)
    val titleEditText = titleView.findViewById<EditText>(R.id.custom_title)
    val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_body)
    titleEditText.setText(card?.question.toString())
    bodyEditText.setText(card?.answer.toString())

    AlertDialog.Builder(viewHolder.itemView.context)
        .setCustomTitle(titleView)
        .setView(bodyView)
        .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
            // do something on click
//            Snackbar.make(viewHolder.itemView, titleEditText.text.toString(), Snackbar.LENGTH_LONG).show()

        viewHolder.flashcardTitle.text=titleEditText.text.toString()
        card?.question=titleEditText.text.toString()
        card?.answer=bodyEditText.text.toString()
        }
        .create()
        .show()
}